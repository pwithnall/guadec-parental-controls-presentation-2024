\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue,
            pdftitle=An update on parental controls for GNOME 47,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage[export]{adjustbox}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}
\usepackage[backend=biber]{biblatex}

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

% Increase the space after the footnotes so that \footfullcite works
\addtobeamertemplate{footline}{\hfill\usebeamertemplate***{navigation symbols}%
    \hspace*{0.1cm}\par\vskip 20pt}{}

\usetheme{guadec}

\AtBeginSection{\frame{\sectionpage}}


% Abstract here: https://events.gnome.org/event/209/contributions/751/
%
% In the GNOME 47 cycle, there have been some updates and renewed work on
% parental controls features and integration across GNOME. This talk will give
% an overview of what’s changed, what it means for apps and users, and future
% plans.


\title{An update on parental controls for GNOME 47}

\author{Philip Withnall\\GNOME Foundation\\\texttt{philip@tecnocode.co.uk}}
\date{July 19, 2024}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\note{25 minutes allocated. 20 minutes for talk, 5 minutes for questions.

Hello, this talk is going to be about parental controls and digital wellbeing
and what’s happened on them in the GNOME 47 cycle so far.
The two are quite closely linked, and as you’ll see, there’s actually been more
work on digital wellbeing this cycle than there has been on parental controls.
That work will form the foundation for later parental controls work.}


\begin{frame}{The plan}
\begin{itemize}
	\item{Health breaks}
	\item{Session time limits / Screen time}
	\item{Bedtime}
\end{itemize}
\end{frame}

\note{The plan for this cycle was to implement these three things, funded by a
grant from Dalio Philanthropies to the GNOME Foundation. They are
useful both for digital wellbeing and for parental controls. For digital
wellbeing, we want to provide tools to help you keep \emph{yourself} well and
happy on the computer. For parental controls, we want to provide tools to help
you help your \emph{children/dependents} keep well and happy on the computer.}


\begin{frame}{Health breaks}
\begin{itemize}
  \item{Various existing apps}
  \item{Tighter integration with the shell}
  \item{Movement breaks}
  \item{Eyesight breaks}
\end{itemize}
\end{frame}

\note{Firstly, health breaks or break reminders. These will function the same for both parents
and children. Their purpose is to periodically remind you to take a break from
the screen, to get you to move and to give your eyes a rest.}

\note{There are various apps which do this already, but by having an
implementation in gnome-shell, we can provide tighter integration with the
desktop and better performance. It means we can use the shell’s internal
activity notifications (for mouse moves) so no IPC is needed to detect the user
being idle. And it means we can display custom overlays or desaturate the screen
to hint to the user that it’s time to take a break. A standalone app, on the
other hand, can only really display notifications.}

\note{The design done by Allan Day gives two kinds of breaks: eyesight breaks,
which are short and frequent; and movement breaks, which are long and
infrequent. The idea is that you look away from the screen during an eyesight
break, and step away from the computer during a movement break.}


\begin{frame}{Digital wellbeing design}
\begin{figure}
	%\includegraphics[width=0.75\columnwidth]{wellbeing.png}
	\adjincludegraphics[width=0.75\columnwidth,trim={0 {.5\height} 0 0},clip]{wellbeing.png}
	\caption{Digital wellbeing design in gnome-control-center (\href{https://gitlab.gnome.org/Teams/Design/settings-mockups/-/blob/master/wellbeing/wellbeing.png}{source})}
\end{figure}
\end{frame}


\begin{frame}{Digital wellbeing design}
\begin{figure}
	%\includegraphics[width=0.75\columnwidth]{wellbeing.png}
	\adjincludegraphics[width=0.75\columnwidth,trim={0 0 0 {.5\height}},clip]{wellbeing.png}
	\caption{Digital wellbeing design in gnome-control-center (\href{https://gitlab.gnome.org/Teams/Design/settings-mockups/-/blob/master/wellbeing/wellbeing.png}{source})}
\end{figure}
\end{frame}

\note{Here’s the design for the full set of digital wellbeing features for
gnome-control-center. What will land in GNOME 47 will likely be a subset of the
options here.}


\begin{frame}{Health breaks notifications}
\begin{figure}
	\includegraphics[width=0.5\columnwidth]{notifications.png}
	\caption{Some health breaks notifications}
\end{figure}
\end{frame}

\note{Frustratingly, for a presentation, there’s fairly little to show of break
reminders. Essentially, they pop up some different notifications at different
times, and give you the option to take, delay or skip a break. A lot of the
implementation complexity comes from tracking your idle/active state and working
out when breaks are upcoming (and which kind of break), and whether you’ve
actually been consistently using the computer long enough to need a break.}


\begin{frame}{Health breaks implementation}
\large{\url{https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3251}}
\end{frame}

\note{Health breaks are implemented! They are currently undergoing code review
but will hopefully land in time for GNOME 47.}


\begin{frame}{Session time limits / Screen time}
\begin{itemize}
  \item{A daily time limit on computer use}
  \item{For you, it’s a hint; for children, it’s mandatory}
\end{itemize}
\end{frame}

\note{Secondly, session time limits or screen time. This is a feature which
behaves similarly but not quite the same between digital wellbeing and parental
controls. For digital wellbeing, we want to provide hints that you’ve spent
enough time on the computer in a day. For parental controls, we want to provide
a mandatory limit on the amount of time a child can spend on the computer.}

\note{In both cases, the user interface for it will be similar — notifications
when you’re almost out of time, and when you are out of time, and either
desaturating the screen (for digital wellbeing), or logging the child out (for
parental controls) when time is up.}


\begin{frame}{Digital wellbeing design}
\begin{figure}
	%\includegraphics[width=0.75\columnwidth]{wellbeing.png}
	\adjincludegraphics[width=0.75\columnwidth,trim={0 {.5\height} 0 0},clip]{wellbeing.png}
	\caption{Digital wellbeing design in gnome-control-center (\href{https://gitlab.gnome.org/Teams/Design/settings-mockups/-/blob/master/wellbeing/wellbeing.png}{source})}
\end{figure}
\end{frame}

\note{Implementation wise, we need to know how long the user has spent logged in
(on any session). We can get this information from systemd in the form of
notifications about when the user’s session state changes, but we then need to
store that history over time. This can then be totalled to give the day’s usage,
and displayed in the graph at the top of the gnome-control-center panel.}

\note{This gets a bit more complex when you consider the digital wellbeing and
parental controls session histories act on different users: in the former case,
the session history is being stored for you; in the latter, for a child user.
The child user may try and tamper with the session history to get more time on
the computer.}

\note{On top of that, a child user might want to set their own digital wellbeing
screen time limit which is lower than the parental controls limit enforced by
their parents.}

\note{So, the session history for digital wellbeing is stored by gnome-shell for
the current user. This is implemented and awaiting review. And the session
history for parental controls will have to be stored by a separate daemon which
will allow the parent user to query it, and not allow the child user to tamper
with it. This is yet to be implemented.}


\begin{frame}{Screen time implementation}
\large{\url{https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3397}}
\end{frame}

\note{The digital wellbeing side of screen time is implemented! It’s currently
undergoing code review but will hopefully land in time for GNOME 47. The
parental controls side of it is not yet implemented.}


\begin{frame}{Bedtime}
\begin{itemize}
  \item{Similar to screen time}
  \item{Reminds you to go to bed at a certain wall clock time}
  \item{Some tie in with night light}
\end{itemize}
\end{frame}

\note{The bedtime feature is what it sounds like: a reminder to stop using the
computer and go to bed at a certain wall clock time. Again, this is something
which can be implemented for both digital wellbeing and parental controls, with
the parental controls version being mandatory.}

\note{GNOME already has a ‘night light’ feature which changes the screen colour
as the sun sets. Bedtime would be additional to this.}

\note{So far, though, this is still a design and has not been implemented yet.
It should be fairly straightforward to implement once the session time limits /
screen time code has landed, as it will reuse much of the same infrastructure.}


\begin{frame}{Miscellany}
\begin{description}
  \item[Digital wellbeing design]{\url{https://gitlab.gnome.org/Teams/Design/settings-mockups/-/blob/master/wellbeing/wellbeing.png}}
  \item[Design details]{\url{https://gitlab.gnome.org/Teams/Design/initiatives/-/issues/130}}
	\item[Slide source]{\url{https://gitlab.gnome.org/pwithnall/guadec-parental-controls-presentation-2024}}
\end{description}


% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}

\vfill
\begin{center}
	\tiny{Beamer theme: \url{https://gitlab.gnome.org/GNOME/presentation-templates/tree/master/GUADEC/2024}}
\end{center}
\end{frame}

\end{document}
